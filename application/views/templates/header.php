<!DOCTYPE html>
<html lang="en">
<title>Warris</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="icon" href="<?php echo base_url(); ?>assets/img/warris.JPG" type="image/JPG" sizes="16x16">
<style>
body {font-family: "Lato", sans-serif}
.mySlides {display: none}
</style>
<body>

<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-black w3-card">
   <a class="w3-bar-item w3-button w3-padding-large w3-hide-medium w3-hide-large w3-right" href="javascript:void(0)" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
    <a href="<?php echo site_url() ; ?>" class="w3-bar-item w3-button w3-padding-large">HOME</a>
    <a href="<?php echo site_url() ; ?>about" class="w3-bar-item w3-button w3-padding-large w3-hide-small">About me</a>
    <a href="<?php echo site_url() ; ?>blog" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Blog</a>
    <a href="<?php echo site_url() ; ?>signup" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Sign Up</a>
    <a href="<?php echo site_url() ; ?>login" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Login</a>
    <a href="<?php echo site_url() ; ?>portfolio/#contact" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Contact Me</a>
    <div class="w3-dropdown-hover w3-hide-small">
      <button class="w3-padding-large w3-button" title="More">MORE <i class="fa fa-caret-down"></i></button>     
      <div class="w3-dropdown-content w3-bar-block w3-card-4">
        <a href="<?php echo site_url() ; ?>food" class="w3-bar-item w3-button">My Favourite Food</a>
        <a href="<?php echo site_url() ; ?>portfolio" class="w3-bar-item w3-button">My Portfolio</a>
        <a href="<?php echo site_url() ; ?>mycv" class="w3-bar-item w3-button">My CV</a>
        
        <a href="#" class="w3-bar-item w3-button">TOUR</a>
      </div>
    </div>
    <a href="javascript:void(0)" class="w3-padding-large w3-hover-red w3-hide-small w3-right"><i class="fa fa-search"></i></a>
  </div>
</div>

<!-- Navbar on small screens (remove the onclick attribute if you want the navbar to always show on top of the content when clicking on the links) -->
<div id="navDemo" class="w3-bar-block w3-black w3-hide w3-hide-large w3-hide-medium w3-top" style="margin-top:46px">
  <a href="<?php echo site_url (); ?>about" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">About me</a>
  <a href="<?php echo site_url() ; ?>blog" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">Blog</a>
  <a href="<?php echo site_url() ; ?>signup" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">Sign Up</a>
  <a href="<?php echo site_url() ; ?>login" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">Login</a>
  <a href="<?php echo site_url() ; ?>portfolio/#contact" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">Contact Me</a>
  <a href="<?php echo site_url() ; ?>food" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">Food</a>
  <a href="<?php echo site_url() ; ?>portfolio" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">My Portfolio</a>
  <a href="<?php echo site_url() ; ?>mycv" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">My CV</a>
  <a href="#" class="w3-bar-item w3-button w3-padding-large" onclick="myFunction()">TOUR</a>
</div>

<script>

 <!--Start of Tawk.to Script-->
 var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
 (function(){
 var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
 s1.async=true;
 s1.src='https://embed.tawk.to/5e7143cd8d24fc2265883a9e/default';
 s1.charset='UTF-8';
 s1.setAttribute('crossorigin','*');
 s0.parentNode.insertBefore(s1,s0);
})();

<!--End of Tawk.to Script-->
// Automatic Slideshow - change image every 4 seconds
var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myIndex++;
  if (myIndex > x.length) {myIndex = 1}    
  x[myIndex-1].style.display = "block";  
  setTimeout(carousel, 4000);    
}

// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// When the user clicks anywhere outside of the modal, close it
var modal = document.getElementById('ticketModal');
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
$(document).ready(function()
{ 
       $(document).bind("contextmenu",function(e){
              return false;
       }); 
})

</script>


</body>
</html>
<br><br><br>