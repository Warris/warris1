<!DOCTYPE html>
<html>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.containermentor {
  position: relative;
 
}

.image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.containermentor:hover .image {
  opacity: 0.3;
}

.containermentor:hover .middle {
  opacity: 1;
}

.text {
  background-color: #000;
  color: white;
  font-size: 25px;
  
}
#quote-carousel {
    padding: 0 10px 30px 10px;
    margin-top: 60px;
}
#quote-carousel .carousel-control {
    background: none;
    color: #CACACA;
    font-size: 2.3em;
    text-shadow: none;
    margin-top: 30px;
}
#quote-carousel .carousel-indicators {
    position: relative;
    right: 50%;
    top: auto;
    bottom: 0px;
    margin-top: 20px;
    margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
    width: 50px;
    height: 50px;
    cursor: pointer;
    border: 1px solid #ccc;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    border-radius: 50%;
    opacity: 0.4;
    overflow: hidden;
    transition: all .4s ease-in;
    vertical-align: middle;
}
#quote-carousel .carousel-indicators .active {
    width: 128px;
    height: 128px;
    opacity: 1;
    transition: all .2s;
}
.item blockquote {
    border-left: none;
    margin: 0;
}
.item blockquote p:before {
    content: "\f10d";
    font-family: 'Fontawesome';
    float: left;
    margin-right: 10px;
}
</style>
<body>

  <!-- Images of Me -->
  <div class="w3-row-padding w3-padding-16" id="about">
    <div class="w3-col m6">
      <img src="<?php echo base_url(); ?>assets/img/warris.JPG" alt="Me" style="width:100%">
    </div>
    <div class="w3-col m6">
      <img src="<?php echo base_url(); ?>assets/img/warris1.JPG" alt="Me" style="width:100%">
    </div>
  </div>

  <div class="w3-container w3-padding-large" style="margin-bottom:32px">
    <h4><b>About Me</b></h4>
    <p>Just me, myself and I, exploring the universe of unknownment. I have a heart of love and an interest of lorem ipsum and mauris neque quam blog. I want to share my world with you. Praesent tincidunt sed tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla. Praesent tincidunt sed tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.</p>
    <hr>
    
    <h4>Technical Skills</h4>
    <!-- Progress bars / Skills -->
    <p>Web development</p>
    <div class="w3-grey">
      <div class="w3-container w3-dark-grey w3-padding w3-center" style="width:95%">95%</div>
    </div>
    <p>Web Design</p>
    <div class="w3-grey">
      <div class="w3-container w3-dark-grey w3-padding w3-center" style="width:85%">85%</div>
    </div>
    <p>Accounting</p>
    <div class="w3-grey">
      <div class="w3-container w3-dark-grey w3-padding w3-center" style="width:80%">80%</div>
    </div>
    
    <hr>

  <!-- About Section -->
  <div class="w3-container w3-padding-32" id="about">
    <h3 class="w3-border-bottom w3-border-light-grey w3-padding-16">About Me </h3>
    <p style="font-size:30px;text-align:center">My Mentors and My Role Model
    </p>
  </div>

  <div class="w3-row-padding w3-grayscale">
  <div class="w3-col l3 m6 w3-margin-bottom">
      <img src="<?php echo base_url(); ?>assets/img/Opeyemi.jpg" alt="Jane" style="width:100%">
      <h3>Ibrahim Opeyemi</h3>
      <p class="w3-opacity">Software Developer</p>
      <p>Ibrahim Opeyemi is my role model,my mentor,my brother and my everything , I believe in World .</p>
      <div class="containermentor" >
      <button class="w3-button w3-light-grey w3-block " class="image" style="width:100%">Contact</button>
      <div class="middle">
    <div class="text" ><a href="https://web.facebook.com/khadreal" target="_blank"> <i class="fa fa-facebook-official w3-hover-opacity"></i></a>
    <a href="https://twitter.com/Khadreal" target="_blank"><i class="fa fa-twitter w3-hover-opacity"></i></a>
    <a href="https://www.linkedin.com/in/opeyemi-ibrahim/" target="_blank"><i class="fa fa-linkedin w3-hover-opacity"></i></a></div>
  </div>
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <img src="<?php echo base_url(); ?>assets/img/Bodun.jpg" alt="Jane" style="width:100%">
      <h3>Akinyele Olubodun</h3>
      <p class="w3-opacity">Software Developer</p>
      <p>Akinyele Olubodun is my mentor and my role model,I meet him through my brother. They mean alot to me </p>
      <div class="containermentor">
      <button class="w3-button w3-light-grey w3-block " class="image" style="width:100%">Contact</button>
      <div class="middle">
    <div class="text" style="padding-left:10px"><a style="background-color:blue" href="https://www.facebook.com/akinyeleolubodun?__tn__=%2CdC-R-R&eid=ARCsVSJkcp7ZcBL9wTAzuOW16gI65EwnVP-4MSxVrXWDGZO3WHvy5vsxRiX_p5ZLs0fqwRRfsbkIEclC&hc_ref=ARRKjWBeFGoLd62zknbjLRShSOy6mmJe-UKcq18yZpCID3q_lOd-hkAngoL225HSLVk&fref=nf"> <i class="fa fa-facebook-official w3-hover-opacity"></i></a>
    <a href="https://twitter.com/olubodunPMJ"><i class="fa fa-twitter w3-hover-opacity"></i></a>
    <a href="https://www.linkedin.com/in/olubodun-akinyele-52825b14/"><i class="fa fa-linkedin w3-hover-opacity"></i></a></div>
  </div>
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <img src="<?php echo base_url(); ?>assets/img/Bodun1.jpg" alt="Jane" style="width:100%">
      <h3>Akinyele Olubodun</h3>
      <p class="w3-opacity">Software Developer</p>
      <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
      <div class="containermentor">
      <button class="w3-button w3-light-grey w3-block " class="image" style="width:100%">Contact</button>
      <div class="middle">
    <div class="text"><a href="https://www.facebook.com/ibrahim.warris58"> <i class="fa fa-facebook-official w3-hover-opacity"></i></a>
    <a href="https://twitter.com/IbrahimWarris"><i class="fa fa-twitter w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-linkedin w3-hover-opacity"></i></a></div>
  </div>
      </div>
    </div>
    <div class="w3-col l3 m6 w3-margin-bottom">
      <img src="<?php echo base_url(); ?>assets/img/Bodun1.jpg" alt="Jane" style="width:100%">
      <h3>Akinyele Olubodun</h3>
      <p class="w3-opacity">Software Developer</p>
      <p>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
      <div class="containermentor">
      <button class="w3-button w3-light-grey w3-block " class="image" style="width:100%">Contact</button>
      <div class="middle">
    <div class="text"><a href="https://www.facebook.com/ibrahim.warris58"> <i class="fa fa-facebook-official w3-hover-opacity"></i></a>
    <a href="https://twitter.com/IbrahimWarris"><i class="fa fa-twitter w3-hover-opacity"></i></a>
    <a href=""><i class="fa fa-linkedin w3-hover-opacity"></i></a></div>
  </div>
      </div>
    </div>
    </div>
    

</html>