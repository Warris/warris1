
<!doctype html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
<meta charset="utf-8">
<style>

.testimonial{
    text-align: center;
}
.demo
{
	padding: 100px 0;
	
}
.heading-title
{
	margin-bottom: 100px;
}
.testimonial .pic{
    width: 85px;
    height: 85px;
    border-radius: 50%;
    margin: 0 auto 40px;
    border: 4px solid #eb7260;
    overflow: hidden;
}

.testimonial .pic img{
    width: 100%;
    height: auto;
}

.testimonial .description{
    color: #8a9aad;
    font-size: 15px;
    font-style: italic;
    line-height: 24px;
    margin-bottom: 20px;
}

.testimonial .testimonial-prof{
    margin:20px 0;
}

.testimonial .title{
    font-size: 20px;
    color: #eb7260;
    margin-right: 20px;
    text-transform: capitalize;
}

.testimonial .title:after{
    content: "";
    margin-left: 30px;
    border-right: 1px solid #808080;
}

.testimonial .testimonial-prof small{
    display: inline-block;
    color: #8a9aad;
    font-size: 17px;
    text-transform: capitalize;
}

.owl-theme .owl-controls .owl-buttons div{
    background: transparent;
    opacity: 1;
}

.owl-buttons{
    position: absolute;
    top: 8%;
    width: 100%;
}

.owl-prev{
    position: absolute;
    left:30%;
}

.owl-next{
    position: absolute;
    right:30%;
}

.owl-prev:after,
.owl-next:after{
    content: "\f060";
    font-family: 'FontAwesome';
    width: 28px;
    height: 28px;
    font-size: 16px;
    color:#808080;
    transition: all 0.15s ease 0s;
}

.owl-next:after{
    content: "\f061";
}

@media only screen and (max-width: 479px){
    .owl-prev{
        left: 10%;
    }
    .owl-next{
        right: 10%;
    }
}@charset "utf-8";
/* CSS Document */

</style>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
 <script>
$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});
	</script>
<title> Testimonial Style </title>
</head>

<body>
 <div class="demo">
        <div class="container">
            <div class="row text-center">
                <h1 class="heading-title"> Testimonial Style </h1>
            </div>

            <div class="row">
                <div class="col-sm-offset-1 col-sm-10">
                    <div id="testimonial-slider" class="owl-carousel">
                        <div class="testimonial">
                            <div class="pic">
                                <img src="<?php echo base_url(); ?>assets/img/warris4.jpg" alt="">
                            </div>
                            <p class="description">
                                Everyone
 I talked to was skeptical about buying livestock online but
Livestock247 made the process transparent, seamless and very convenient.
 I ordered everything remotely and a vet; who is better qualified at
identifying healthy animals than myself was dispatched to procure the
animals. They had contacts in different meat markets across the nation
and made enquiries to get the best deals for me. They sent real-time
videos to me to approve the livestock while still in the market,
delivered right to my house in the village and scanned the electronic ID
 chips on the animals on video after delivery to confirm that they were
the same ones I had chosen. Everything was handled very professionally
and at very reasonable prices too.
                            </p>
                            <p style="text-align:center;color:red">Minna Bassey</p>
                        </div>

                        <div class="testimonial">
                            <div class="pic">
                                <img src="<?php echo base_url(); ?>assets/img/warris4.jpg" alt="">
                            </div>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lobortis odio ac eros sollicitudin consequat. Proin a tortor tortor. Nullam consequat dictum metus in vulputate. Pellentesque congue lectus justo, et.
                            </p>
                            <div class="testimonial-prof">
                                <span class="title">kristiana</span>
                                <small>Web Designer</small>
                            </div>
                        </div>

                        <div class="testimonial">
                            <div class="pic">
                                <img src="<?php echo base_url(); ?>assets/img/warris4.jpg" alt="">
                            </div>
                            <p class="description">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lobortis odio ac eros sollicitudin consequat. Proin a tortor tortor. Nullam consequat dictum metus in vulputate. Pellentesque congue lectus justo, et.
                            </p>
                            <div class="testimonial-prof">
                                <span class="title">Steve Thomas</span>
                                <small>Web Developer</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
</html>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Carousel Example</h2>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="https://livestock247.com/connectors/system/phpthumb.php?src=images%2Fimage.png&w=512&h=0&HTTP_MODAUTH=modx5cb3aff1e46863.62054981_15e79bfaf766694.66316325&f=png&q=90&wctx=mgr&source=1&t=1584631109" alt="Los Angeles" style="width:100%;height:500px">
        <div class="carousel-caption" style="margin-top:500px">
          <h3 style="color:red">Mrs Bassey</h3>
          <p style="color:#2078BF;">Everyone
 I talked to was skeptical about buying livestock online but
Livestock247 made the process transparent, seamless and very convenient. They sent real-time
videos to me to approve the livestock while still in the market,
delivered right to my house in the village and scanned the electronic ID
 chips on the animals on video after delivery to confirm that they were
the same ones I had chosen. Everything was handled very professionally
and at very reasonable prices too.</p>
        </div>
      </div>

      <div class="item">
        <img src="chicago.jpg" alt="Chicago" style="width:10%;height:500px">
        <div class="carousel-caption">
          <h3>Chicago</h3>
          <p>Thank you, Chicago!</p>
        </div>
      </div>
    
      <div class="item">
        <img src="ny.jpg" alt="New York" style="width:100%;">
        <div class="carousel-caption">
          <h3>New York</h3>
          <p>We love the Big Apple!</p>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

</body>
</html>

