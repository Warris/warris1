


<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

</script>
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}
#g-recaptcha-response {
    display: block !important;
    position: absolute;
    margin: -78px 0 0 0 !important;
    width: 302px !important;
    height: 76px !important;
    z-index: -999999;
    opacity: 0;
}
</style>
<body class="w3-light-grey w3-content" style="max-width:1600px">
<br>
<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
  <div class="w3-container">
    <a href="#" onclick="w3_close()" class="w3-hide-large w3-right w3-jumbo w3-padding w3-hover-grey" title="close menu">
      <i class="fa fa-remove"></i>
    </a>
    <img src="<?php echo base_url(); ?>assets/img/warris.JPG" style="width:45%;" class="w3-round"><br><br>
    <h4><b>MY PORTFOLIO</b></h4>
    <p class="w3-text-grey">Ibrahim Warris</p>
  </div>
  <div class="w3-bar-block">
    <a href="#portfolio" onclick="w3_close()" class="w3-bar-item w3-button w3-padding w3-text-teal"><i class="fa fa-th-large fa-fw w3-margin-right"></i>PORTFOLIO</a> 
    <a href="#about" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fa fa-user fa-fw w3-margin-right"></i>ABOUT</a> 
    <a href="<?php echo base_url('portfolio/#contact'); ?>" onclick="w3_close()" class="w3-bar-item w3-button w3-padding"><i class="fa fa-envelope fa-fw w3-margin-right"></i>CONTACT</a>
  </div>
  <div class="w3-panel w3-large">
  <a href="https://www.facebook.com/ibrahim.warris58"><i class="fa fa-facebook-official w3-hover-opacity"></i></a>
  <a href="https://www.instagram.com/ibrahimwarris_original/?hl=en"> <i class="fa fa-instagram w3-hover-opacity"></i></a>
    <i class="fa fa-snapchat w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p w3-hover-opacity"></i>
  <a href="https://twitter.com/IbrahimWarris"> <i class="fa fa-twitter w3-hover-opacity"></i></a>
    <i class="fa fa-linkedin w3-hover-opacity"></i>
  </div>
</nav>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px">

  <!-- Header -->
  <header id="portfolio">
    <a href=""><img src="/img/warris.JPG" style="width:65px;" class="w3-circle w3-right w3-margin w3-hide-large w3-hover-opacity"></a>
    <span class="w3-button w3-hide-large w3-xxlarge w3-hover-text-grey" onclick="w3_open()"><i class="fa fa-bars"></i></span>
    <div class="w3-container">
    <h1><b>My Portfolio</b></h1>
    <div class="w3-section w3-bottombar w3-padding-16">
      <span class="w3-margin-right">Filter:</span>
      <button class="w3-button w3-black">ALL</button>
      <button class="w3-button w3-white"><i class="fa fa-diamond w3-margin-right"></i>Design</button>
      <button class="w3-button w3-white w3-hide-small"><i class="fa fa-photo w3-margin-right"></i>Photos</button>
      <button class="w3-button w3-white w3-hide-small"><i class="fa fa-map-pin w3-margin-right"></i>Art</button>
    </div>
    </div>
  </header>

  <!-- First Photo Grid-->
  <div class="w3-row-padding">
    <div class="w3-third w3-container w3-margin-bottom">
      <b>Name</b>
      <div class="w3-container w3-white">
        <p><b></b></p>
        <p>I'm Ibrahim Warris ,a web developer solving problems creating a dynamic and user friendly web application   .</p>
      </div>
    </div>
    <div class="w3-third w3-container w3-margin-bottom">
      <b>Skills</b>
      <div class="w3-container w3-white">
        <p><b></b></p>
        <p>HTML5 , CSS3 , JAVASCRIPT  , <BR>PYTHON , <BR>PHP.</p>
      </div>
    </div>
    <div class="w3-third w3-container w3-margin-bottom">
      <b>FrameWorks</b>
      <div class="w3-container w3-white">
        <p><b></b></p>
        <p>Laravel<br>Codeigniter<br>Bootstrap.</p>
      </div>
    </div>

  </div>

  <!-- Second Photo Grid-->
  <div class="w3-row-padding">
  <div class="w3-third w3-container w3-margin-bottom">
    <b>Librarys</b>
    <div class="w3-container w3-white">
      <p>JQUERY<BR>REACT.</p>
    </div>
  </div>
  <div class="w3-third w3-container w3-margin-bottom">
    <b>Hobbies</b>
    <div class="w3-container w3-white">
      <p>I love Watching and Playing football,Watching Educative movies.</p>
    </div>
  </div>


  </div>

    <p>
       <a href="https://drive.google.com/file/d/1fRUW-T5-guUkk_axee9YKlYNNq5jf0Mg/view?usp=sharing"><button class="w3-button w3-dark-grey w3-padding-large w3-margin-top w3-margin-bottom">
        <i class="fa fa-download w3-margin-right"></i>Preview Resume(CV)
      </button></a>
    </p>
    <hr>

  
  <!-- Contact Section -->
  <div class="w3-container w3-padding-large w3-grey" >
    <h4 id="contact"><b>Contact Me</b></h4>
    <div class="w3-row-padding w3-center w3-padding-24" style="margin:0 -16px">
      <div class="w3-third w3-dark-grey">
        <p><i class="fa fa-envelope w3-xxlarge w3-text-light-grey"></i></p>
        <p>warris.ibrahim@livestock247.com</p>
      </div>
      <div class="w3-third w3-teal">
        <p><i class="fa fa-map-marker w3-xxlarge w3-text-light-grey"></i></p>
        <p>Lagos, Nigeria</p>
      </div>
      <div class="w3-third w3-dark-grey">
        <p><i class="fa fa-phone w3-xxlarge w3-text-light-grey"></i></p>
        <p>09025670847</p>
      </div>
    </div>
    <hr class="w3-opacity">
    <form action="<?=base_url('email')?>"  target="_blank" method="post" >
      <div class="w3-section">
        <label>Your Name</label>
        <input class="w3-input w3-border" type="text" name="subject" required>
      </div>
      <div class="w3-section">
        <label>Your Email</label>
        <input class="w3-input w3-border" type="text" name="to" required>
      </div>
      <div class="w3-section">
        <label>Message</label>
        <input class="w3-input w3-border" type="text" name="message" required>
      </div>
      <div class="w3-section">
        <label>Message1</label>
        <input class="w3-input w3-border" type="text" name="message1" required>
      </div>
     
      <br/>
      <button type="submit" class="w3-button w3-black w3-margin-bottom"><i class="fa fa-paper-plane w3-margin-right"></i>Send Message</button>
      <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
    async defer ></script>
    

    
    
    </form>
    
  </div>

 <!-- Footer -->
 <footer class="w3-row-padding w3-padding-32">
    <div class="w3-third">
      <h3>ABOUT ME</h3>
      <p>Just me, myself and I, exploring the universe of uknownment. I have a heart of love and a interest of the world . I want to share my world with you..</p>
      <p>Powered by <a href="<?php echo base_url('about'); ?>" target="_blank">About</a></p>
    </div>
  
    <div class="w3-third">
      <h3>POPULAR POSTS</h3>
        <ul class="w3-ul w3-hoverable ">
      <li class="w3-padding-16">
        <img src="<?php echo base_url(); ?>assets/img/workshop.jpg" alt="Image" class="w3-left w3-margin-right" style="width:50px">
        <span class="w3-large">My World</span><br>
        <span>I believe in My World</span>
      </li>
      <li class="w3-padding-16">
        <img src="<?php echo base_url(); ?>assets/img/gondol.jpg" alt="Image" class="w3-left w3-margin-right" style="width:50px">
        <span class="w3-large">Never Give Up</span><br>
        <span>Stay focus and pursue your dreams</span>
      </li> 
      </ul>
    </div>

    <div class="w3-third w3-serif">
      <h3>POPULAR TAGS</h3>
      <p>
        <span class="w3-tag w3-black w3-margin-bottom">Travel</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">New York</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Dinner</span>
        <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Salmon</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">France</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Drinks</span>
        <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Ideas</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Flavors</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Cuisine</span>
        <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Chicken</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Dressing</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Fried</span>
        <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Fish</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Duck</span>
      </p>
    </div>
  </footer>
  
  <div class="w3-black w3-center w3-padding-24">Powered by <a href="<?php echo base_url(); ?>" title="W3.CSS" target="_blank" class="w3-hover-opacity">Ibrahim Warris</a></div>

<!-- End page content -->
</div>

<script>
// Script to open and close sidebar
function w3_open() {
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("myOverlay").style.display = "block";
}
 
function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    document.getElementById("myOverlay").style.display = "none";
}




//window.onload = function() {
   // var $recaptcha = document.querySelector('#g-recaptcha-response');

   // if($recaptcha) {
  //      $recaptcha.setAttribute("required", "required");
  
  
  
  // the div for recaptcha (that is suppose to be in the html tag)  <div class="g-recaptcha"  data-sitekey="6Ldkvt8UAAAAAOD-AEOyEdLv8L7EvY4kN4pPSAzA"></div>
  
  
  
   // }
//};
$( '#btn-validate' ).click(function(){
  var $captcha = $( '#recaptcha' ),
      response = grecaptcha.getResponse();
  
  if (response.length === 0) {
    $( '.msg-error').text( "reCAPTCHA is mandatory" );
    if( !$captcha.hasClass( "error" ) ){
      $captcha.addClass( "error" );
    }
  } else {
    $( '.msg-error' ).text('');
    $captcha.removeClass( "error" );
    alert( 'reCAPTCHA marked' );
  }
})
</script>








</body>
</html>
