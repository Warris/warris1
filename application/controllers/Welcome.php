<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		$this->load->view('templates/header');
		$this->load->view('project/home');
		$this->load->view('templates/footer');
		
	}

public function signup()
	{
		$this->load->view('templates/header');
		
		
		//load database 
	//$this->load->database();
			//load Model
	$this->load->model('Project_Model');
		// validation
        $this->load->library('form_validation');
	    $this->form_validation->set_rules('dname', 'Username', 'required');
       $this->form_validation->set_rules('dmobile', 'Mobile No.', 'required');

	if ($this->form_validation->run() == FALSE) {
		$this->load->view('project/signup');
		} else {
			
	
		//Check submit button 
		if($this->input->post('save') )
		{
			if ( $_POST['dpass'] == $_POST['dpass1']) {
				# code...
			
			//load registration view form
		$this->load->view('project/signup');
		//get form's data and store in local varable
		$dname=$this->input->post('dname');
		$dmobile=$this->input->post('dmobile');
		$demail=$this->input->post('demail');
		$pass=$this->input->post('dpass');
		
//call saverecords method of Project_Model and pass variables as parameter
		$this->Project_Model->insert_into_db();		
		echo "
		<script>
		alert('Record saved successfully')
		</script>";
	}
	else {
		redirect(base_url().project);
	}
		}
		}
		$this->load->view('templates/footer');
		}




		public function login()
	{
		$this->load->view('templates/header');
		$this->load->view('project/login');

		
		$this->load->view('templates/footer');
		
		
	}


	public function process(){
        // Load the model
        $this->load->model('Project_Model');
        // Validate the user can login
        $result = $this->login_model->validate();
        // Now we verify the result
        if(! $result){
            // If user did not validate, then show them login page again
            $this->index();
        }else{
            // If user did validate, 
            // Send them to members area
            redirect('');
        }        
	}
	private function check_isvalidated(){
        if(! $this->session->userdata('validated')){
            redirect('login');
		}
	}
	
		




	public function blog()
	{
		$this->load->view('templates/header');
		$this->load->view('project/blog');
		$this->load->view('templates/footer');
		
		
	}

	public function about()
	{
		$this->load->view('templates/header');
		$this->load->view('project/about');
		$this->load->view('templates/footer');
		
		
	}

	public function food()
	{
		$this->load->view('templates/header');
		$this->load->view('project/food');
		$this->load->view('templates/footer');
		
		
	}

	public function mycv()
	{
		$this->load->view('templates/header');
		$this->load->view('project/mycv');
		$this->load->view('templates/footer');
		
		
	}
	public function portfolio()
	{
		$this->load->view('templates/header');
		$this->load->view('project/portfolio');
		$this->load->view('templates/footer');
		
		
	}

	public function sendemail()
	{
		$this->load->view('templates/header');
		$this->load->view('project/send-email');
		$this->load->view('templates/footer');
		
		
	}







	public function message()
	{
		/*load registration view form*/
		$this->load->view('project/message');
	
		/*Check submit button */
		if($this->input->post('save'))
		{
		$phone=$this->input->post(‘phone’);
$user_message=$this->input->post(‘message’);
	    /*Your authentication key*/
$authKey = "3456655757gEr5a019b18";
/*Multiple mobiles numbers separated by comma*/
$mobileNumber = $phone;
/*Sender ID,While using route4 sender id should be 6 characters long.*/
$senderId = "ABCDEF";
/*Your message to send, Add URL encoding here.*/
$message = $user_message;
/*Define route */
$route = "route=4";
/*Prepare you post parameters*/
$postData = array(
'authkey' => $authKey,
'mobiles' => $mobileNumber,
'message' => $message,
'sender' => $senderId,
'route' => $route
);
/*API URL*/
$url="https://control.msg91.com/api/sendhttp.php";
/* init the resource */
$ch = curl_init();
curl_setopt_array($ch, array(
CURLOPT_URL => $url,
CURLOPT_RETURNTRANSFER => true,
CURLOPT_POST => true,
CURLOPT_POSTFIELDS => $postData
/*,CURLOPT_FOLLOWLOCATION => true*/
));
/*Ignore SSL certificate verification*/
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
/*get response*/
$output = curl_exec($ch);
/*Print error if any*/
if(curl_errno($ch))
{
echo 'error:' . curl_error($ch);
}
curl_close($ch);
		
		
			
		echo "Message Sent Successfully !";
		}
	}



	function contactme() {
        $this->load->config('email');
        $this->load->library('email');
        
        $to = $this->config->item('smtp_user');
        $from = $this->input->post('to');
        
        $subject = $this->input->post('subject');
        $msg = $this->input->post('message');
        $mafo='ibrahimwarris2002@yahoo.com';
        
        $message =" Hi Warris,
My name is  $subject
This is my email $from
i just want to ask about $msg ";
        

        $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($to);
        $this->email->to($mafo);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->load->view('templates/header');

        if ($this->email->send()) {
            
		
	
            echo '<br><br><br><br><br><div style=" padding: 20px;
  background-color: #f44336;
  color: white;
  opacity: 1;
  transition: opacity 0.6s;
  margin-bottom: 15px;background-color: #4CAF50;">
  <span style="margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;">&times;</span>  
<strong>Success!</strong> Your message is sent successfully (Received) . To get  instant reply <a href="<?php echo base_url() ; ?>" >Beep Me</a>
</div><script>
var close = document.getElementsByClassName("closebtn");
var i;

for (i = 0; i < close.length; i++) {
  close[i].onclick = function(){
    var div = this.parentElement;
    div.style.opacity = "0";
    setTimeout(function(){ div.style.display = "none"; }, 600);
  }
}
</script>';
            
            	$this->load->view('templates/footer');
        } else {
            show_error($this->email->print_debugger());
        }
	}
	





}

